/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package text;

import java.util.ArrayList;

/**
 *
 * @author DM
 */
public class Split {

    public static ArrayList<String> strToTweet(String text){
        Clause clause = new Clause();        
        ArrayList<String> list = new ArrayList<>();
        String tweet;
        int pos;
      
        boolean lastTweet = false;
        do {
            if (clause.isFinal(text)) { //Is the last Tweet (or only 1?)
                lastTweet = true;
                tweet = text;
            } else {
                pos = clause.getCitationPos(text);
                if (pos==-1)                
                    pos = clause.getPhrasePos(text);
                if (pos==-1)
                    pos = clause.getWordPos(text);
                if (pos==-1)
                    pos = clause.getFinalPos(text);
                tweet = text.substring(0,pos);
                text = text.substring(pos);
            }
            list.add(tweet);            
        } while (!lastTweet);
        
        return list;
    }
    
}
